<?php

/**
 * @file
 * Views Slideshow: Template for text control - previous.
 *
 * - $variables: Contains theme variables.
 * - $classes: Text control classes.
 * - $vss_id: The Views Slideshow unique ID.
 *
 * @ingroup vss_templates
 */
 $previous = '<button style="font-size:17px"><i class="fa fa-backward"></i></button>';
?>
<span id="views_slideshow_controls_text_previous_<?php print $vss_id; ?>" class="<?php print $classes; ?>">
  <a href="#" rel="prev"><?php print t($previous); ?></a>
</span>

