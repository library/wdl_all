<?php

/**
 * @file
 * Views Slideshow: Template for text control - next.
 *
 * - $variables: Contains theme variables.
 * - $classes: Text control classes.
 * - $vss_id: The Views Slideshow unique id.
 *
 * @ingroup vss_templates
 */
 $next = '<button style="font-size:17px"><i class="fa fa-forward"></i></button>';
?>
<span id="views_slideshow_controls_text_next_<?php print $vss_id; ?>" class="<?php print $classes; ?>">
  <a href="#" rel="next"><?php print t($next); ?></a>
</span>

