<?php

/**
 * @file
 * Views Slideshow: Template for text control - pause/start.
 *
 * - $variables: Contains theme variables.
 * - $classes: Text control classes.
 * - $start_text: Start/Pause control text.
 *
 * @ingroup vss_templates
 */


  $play = '<button style="font-size:17px"><i class="fa fa-play"></i></button>';
  $pause = '<button style="font-size:17px"><i class="fa fa-pause"></i></button>';

  if(strtolower($start_text) == 'pause') {
    $start_text = $pause;
  }elseif(strtolower($start_text) == 'resume') {
    $start_text = $play;
  }else{
    $start_text = '';
  }
?>
<span id="views_slideshow_controls_text_pause_<?php print $variables['vss_id']; ?>" class="<?php print $classes; ?>"><a href="#"><?php print $start_text; ?></a></span>

